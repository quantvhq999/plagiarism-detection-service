-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th7 25, 2021 lúc 05:09 PM
-- Phiên bản máy phục vụ: 10.4.17-MariaDB
-- Phiên bản PHP: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `plagiarism`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `exer`
--

CREATE TABLE `exer` (
  `id_ex` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_vietnamese_ci NOT NULL,
  `id_sub` int(11) NOT NULL,
  `date` date NOT NULL,
  `content` text COLLATE utf8_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Đang đổ dữ liệu cho bảng `exer`
--

INSERT INTO `exer` (`id_ex`, `name`, `id_sub`, `date`, `content`) VALUES
(1, 'Bài tập 1', 1, '2021-07-22', 'Tìm hiểu abc'),
(2, 'Thực hành số 1', 2, '2021-07-25', 'Thực hành XML'),
(3, 'Thực hành số 2', 2, '2021-07-29', 'Thực hành ABC'),
(4, 'Bài tập 2', 2, '2021-07-21', 'Bài tập thực hành thứ n');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `homework`
--

CREATE TABLE `homework` (
  `id` int(11) NOT NULL,
  `id_sv` int(11) NOT NULL,
  `id_ex` int(11) NOT NULL,
  `date` date NOT NULL,
  `file` text COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `done` tinyint(1) NOT NULL DEFAULT 0,
  `late` tinyint(1) NOT NULL DEFAULT 0,
  `id_sub` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `library`
--

CREATE TABLE `library` (
  `id_lib` int(11) NOT NULL,
  `subject_code` varchar(15) COLLATE utf8_vietnamese_ci NOT NULL,
  `file` text COLLATE utf8_vietnamese_ci NOT NULL,
  `author` varchar(50) COLLATE utf8_vietnamese_ci NOT NULL,
  `date` date NOT NULL,
  `name` varchar(100) COLLATE utf8_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Đang đổ dữ liệu cho bảng `library`
--

INSERT INTO `library` (`id_lib`, `subject_code`, `file`, `author`, `date`, `name`) VALUES
(15, 'DATAMIN', 'Quả địa cầu có 4 đại dương, dương dương là cái giường để ngủ', 'Abc', '2021-07-20', 'Ca dao Việt Nam'),
(17, 'WEB', 'JavaScript is the world\'s most popular programming language.\r\nJavaScript is the programming language of the Web.\r\n\r\nJavaScript is easy to learn.\r\n\r\nThis tutorial will teach you JavaScript from basic to advanced.', 'Javascript', '2021-07-13', 'Ngôn ngữ Javascript'),
(18, 'SONG', 'Where there\'s a will, there\'s a way, kind of beautiful\r\nAnd every night has its day, so magical\r\nAnd if there\'s love in this life, there\'s no obstacle\r\nThat can\'t be defeated\r\n\r\nFor every tyrant, a tear for the vulnerable\r\nIn every lost soul, the bones of a miracle\r\nFor every dreamer, a dream, we\'re unstoppable\r\nWith something to believe in\r\n\r\nMonday left me broken\r\nTuesday, I was through with hoping\r\nWednesday, my empty arms were open\r\nThursday, waiting for love, waiting for love\r\n\r\nThank the stars it\'s Friday\r\nI\'m burning like a fire gone wild on Saturday\r\nGuess I won\'t be coming to church on Sunday\r\nI\'ll be waiting for love, waiting for love to come around', 'Avicii', '2015-06-26', 'Waiting For Love'),
(19, 'DATAMIN', 'A horse-racing gambler, hoping to maximize his winnings, decides to create a computer program\r\nthat will accurately predict the winner of a horse race based on the usual information (number of\r\nraces recently won by each horse, betting odds for each horse, etc.). To create such a program, he\r\nasks a highly successful expert gambler to explain his betting strategy. Not surprisingly, the expert\r\nis unable to articulate a grand set of rules for selecting a horse. On the other hand, when presented\r\nwith the data for a specific set of races, the expert has no trouble coming up with a “rule of thumb”\r\nfor that set of races (such as, “Bet on the horse that has recently won the most races” or “Bet on\r\nthe horse with the most favored odds”). Although such a rule of thumb, by itself, is obviously very\r\nrough and inaccurate, it is not unreasonable to expect it to provide predictions that are at least a\r\nlittle bit better than random guessing. Furthermore, by repeatedly asking the expert’s opinion on\r\ndifferent collections of races, the gambler is able to extract many rules of thumb.\r\nIn order to use these rules of thumb to maximum advantage, there are two problems faced by\r\nthe gambler: First, how should he choose the collections of races presented to the expert so as to\r\nextract rules of thumb from the expert that will be the most useful? Second, once he has collected\r\nmany rules of thumb, how can they be combined into a single, highly accurate prediction rule?\r\n\r\nBoosting refers to a general and provably effective method of producing a very accurate pre-\r\ndiction rule by combining rough and moderately inaccurate rules of thumb in a manner similar to\r\n\r\nthat suggested above. This short paper overviews some of the recent work on boosting, focusing\r\nespecially on the AdaBoost algorithm which has undergone intense theoretical study and empirical\r\ntesting. After introducing AdaBoost, we describe some of the basic underlying theory of boosting,\r\nincluding an explanation of why it often tends not to overfit. We also describe some experiments\r\nand applications using boosting.', 'Steven Abney', '1999-07-07', 'A Short Introduction to Boosting'),
(20, 'CD1', 'XML là từ viết tắt của từ Extensible Markup Language là ngôn ngữ đánh dấu mở rộng. XML có chức năng truyền dữ liệu và mô tả nhiều loại dữ liệu khác nhau. Tác dụng chính của XML là đơn giản hóa việc chia sẻ dữ liệu giữa các nền tảng và các hệ thống được kết nối thông qua mạng Internet.\r\n\r\nXML dùng để cấu trúc, lưu trữ và trong trao đổi dữ liệu giữa các ứng dụng và lưu trữ dữ liệu. Ví dụ khi ta xây dựng một ứng dụng bằng Php và một ứng dụng bằng Java thì hai ngôn ngữ này không thể hiểu nhau, vì vậy ta sẽ sử dụng XML để trao đổi dữ liệu. Chính vì vậy, XML có tác dụng rất lớn trong việc chia sẻ, trao đổi dữ liệu giữa các hệ thống.', 'Cô Hạnh', '2021-07-17', 'XML là gì');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `subject`
--

CREATE TABLE `subject` (
  `id_sub` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_vietnamese_ci NOT NULL,
  `subject_code` varchar(10) COLLATE utf8_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Đang đổ dữ liệu cho bảng `subject`
--

INSERT INTO `subject` (`id_sub`, `name`, `subject_code`) VALUES
(1, 'Khai phá dữ liệu', 'DATAMIN'),
(2, 'Chuyên đề 1', 'CD1');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user`
--

CREATE TABLE `user` (
  `id_sv` int(11) NOT NULL,
  `std_id` varchar(10) COLLATE utf8_vietnamese_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_vietnamese_ci NOT NULL,
  `birthday` date NOT NULL,
  `gender` varchar(5) COLLATE utf8_vietnamese_ci NOT NULL,
  `address` varchar(50) COLLATE utf8_vietnamese_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_vietnamese_ci NOT NULL,
  `cmnd` int(9) NOT NULL,
  `dad_name` varchar(50) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `mom_name` varchar(50) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `year` varchar(50) COLLATE utf8_vietnamese_ci NOT NULL,
  `class` varchar(10) COLLATE utf8_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Đang đổ dữ liệu cho bảng `user`
--

INSERT INTO `user` (`id_sv`, `std_id`, `name`, `birthday`, `gender`, `address`, `password`, `cmnd`, `dad_name`, `mom_name`, `year`, `class`) VALUES
(1, '0000000001', 'VƯƠNG HOÀNG BẢO', '1999-01-01', 'Nữ', 'Bình Hoà, Thuận An, Bình Dương', '01/01/1999', 281123914, 'Vương Hoàng A', 'Nguyễn Thị B', '2017-2021', 'S20-59TH'),
(2, '0000000002', 'TRƯƠNG VĂN TEST', '2021-07-15', 'Nữ', 'Bình Hoà, Thuận An, Bình Dương', '01/10/2021', 123, 'Trương Văn A', 'Nguyễn Thị B', '2017-2021', 'S20-59TH'),
(12, '1', 'TRƯƠNG VĂN TEST 1', '2021-07-19', 'Nữ', 'Bình Hoà, Thuận An, Bình Dương', '1', 1234, 'Trương Văn A', 'Nguyễn Thị B', '2017-2021', 'S20-59TH');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `exer`
--
ALTER TABLE `exer`
  ADD PRIMARY KEY (`id_ex`),
  ADD KEY `id_sub` (`id_sub`);

--
-- Chỉ mục cho bảng `homework`
--
ALTER TABLE `homework`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mssv` (`id_sv`),
  ADD KEY `id_ex` (`id_ex`),
  ADD KEY `id_sub` (`id_sub`);

--
-- Chỉ mục cho bảng `library`
--
ALTER TABLE `library`
  ADD PRIMARY KEY (`id_lib`);

--
-- Chỉ mục cho bảng `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`id_sub`);

--
-- Chỉ mục cho bảng `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_sv`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `exer`
--
ALTER TABLE `exer`
  MODIFY `id_ex` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `homework`
--
ALTER TABLE `homework`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT cho bảng `library`
--
ALTER TABLE `library`
  MODIFY `id_lib` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT cho bảng `subject`
--
ALTER TABLE `subject`
  MODIFY `id_sub` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `user`
--
ALTER TABLE `user`
  MODIFY `id_sv` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `exer`
--
ALTER TABLE `exer`
  ADD CONSTRAINT `exer_ibfk_1` FOREIGN KEY (`id_sub`) REFERENCES `subject` (`id_sub`);

--
-- Các ràng buộc cho bảng `homework`
--
ALTER TABLE `homework`
  ADD CONSTRAINT `homework_ibfk_1` FOREIGN KEY (`id_sv`) REFERENCES `user` (`id_sv`),
  ADD CONSTRAINT `homework_ibfk_2` FOREIGN KEY (`id_ex`) REFERENCES `exer` (`id_ex`),
  ADD CONSTRAINT `homework_ibfk_3` FOREIGN KEY (`id_sub`) REFERENCES `subject` (`id_sub`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
