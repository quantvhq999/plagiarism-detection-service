export const state = ()=>({
    test: null,
    file:null
})

export const mutations = {
    SET_TEST(state,test){
        state.test = test
    }
}

export const getters = {
    getterTestResult(state){
        if(state.test == null){
            let a = 0
            return a++
        }else{
            const test = state.test
            return test
        }
    }
}

export const actions = {
    async getTestResult({commit},text){
        const result = await this.$axios.$post('http://localhost:5000/test',text)
        commit('SET_TEST',result)
    }
}
