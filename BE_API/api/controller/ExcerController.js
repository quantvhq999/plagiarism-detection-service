'use strict'

const util = require('util')
const mysql = require('mysql')
const db = require('./../db')
const { response } = require('express')

module.exports = {
    get: (req, res) => {
        let sql = "SELECT exer.* , homework.done, homework.late FROM exer LEFT JOIN homework ON exer.id_ex = homework.id_ex"
        db.query(sql,(err,response)=>{
            if( err) throw err
            res.json(response)
        })
    },
    detail: (req, res) => {
        let id_sub = req.params.id_sub;
        let sql = 'SELECT exer.* , homework.done, homework.late FROM exer LEFT JOIN homework ON exer.id_ex = homework.id_ex WHERE exer.`id_sub`= ?'
        if (!id_sub) {
            return res.status(400).send({ message: 'Please provide library id' });
        }
        db.query(sql, id_sub, function (error, response, fields) {
            if (error) throw error;
            return res.json(response)
        });
    },
}