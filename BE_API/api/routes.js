'use strict';

//library routed
module.exports = (app) =>{
  let librariesCtrl = require('./controller/LibrariesController')
  app.route('/library')
    .get(librariesCtrl.get)
    .post(librariesCtrl.store)

  app.route('/library/:subject_code')
    .get(librariesCtrl.detail)
    .put(librariesCtrl.update)
    .delete(librariesCtrl.delete)
  
  let userCtrl = require('./controller/UserControlller')
  app.route('/user')
    .get(userCtrl.get)
    .post(userCtrl.store)
  app.route('/user/:UserId')
    .get(userCtrl.detail)
  app.route('/login')
    .post(userCtrl.login)

  let subjectCtrl = require('./controller/SubjectController')
  app.route('/subject')
    .get(subjectCtrl.get)
  app.route('/subject/:id_sub')
    .get(subjectCtrl.detail)

  let homeworkCtrl = require('./controller/HomeworkController')
  app.route('/homework')
    .get(homeworkCtrl.all)

  app.route('/homework/:id_ex')
    .get(homeworkCtrl.get)
    .post(homeworkCtrl.submit)

  let exerCtrl = require('./controller/ExcerController')
  app.route('/exer')
    .get(exerCtrl.get)
    app.route('/exer/:id_sub')
    .get(exerCtrl.detail)

  let testCtrl = require('./controller/TestDocumentController')
  app.route('/test').post(testCtrl.test)
};

