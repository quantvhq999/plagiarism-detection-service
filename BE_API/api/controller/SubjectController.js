'use strict'

const util = require('util')
const mysql = require('mysql')
const db = require('./../db')
const { response } = require('express')

module.exports = {
    get:(req, res)=>{
        let sql = "SELECT * FROM subject"
        db.query(sql, (err, response) => {
            if (err) throw err
            res.json(response)
        })
    },
    detail:(req,res)=>{
        let sql = "SELECT * FROM subject WHERE id_sub = ?"
        db.query(sql, [req.params.id_sub], (err, response) => {
            if (err) throw err
            res.json(response[0])
        })
    }
}