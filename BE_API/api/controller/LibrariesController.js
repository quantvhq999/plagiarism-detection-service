'use strict'

const util = require('util')
const mysql = require('mysql')
const db = require('./../db')
const { response } = require('express')

module.exports = {
    get: (req, res) => {
        let sql = 'SELECT * FROM library'
        db.query(sql, (err, response) => {
            if (err) throw err
            res.json(response)
        })
    },
    detail: (req, res) => {
        let id_lib = req.params.subject_code;
        let sql = 'SELECT * FROM `library` WHERE `subject_code` = ?'
        if (!id_lib) {
            return res.status(400).send({ message: 'Please provide library id' });
        }
        db.query(sql, id_lib, function (error, response, fields) {
            if (error) throw error;
            return res.json(response)
        });
    },
    update: (req, res) => {
        let data = req.body;
        let libraryId = req.params.libraryId;
        let sql = 'UPDATE library SET ? WHERE id_lib = ?'
        db.query(sql, [data, libraryId], (err, response) => {
            if (err) throw err
            res.json({ message: 'Update success!' })
        })
    },
    store: (req, res) => {
        let data = req.body;
        let sql = 'INSERT INTO library SET ?'
        db.query(sql, [data], (err, response) => {
            if (err) throw err
            res.json({ message: 'Insert success!' })
        })
    },
    delete: (req, res) => {
        let sql = 'DELETE FROM library WHERE id_lib = ?'
        db.query(sql, [req.params.id_lib], (err, response) => {
            if (err) throw err
            res.json({ message: 'Delete success!' })
        })
    }
}