export const state = () => ({
    login: null
})

export const mutations = {
    SET_LOGIN(state, message) {
        state.login = message
    }
}

export const getters = {
    getterLoginMessage(state) {
        const message = state.login
        return message
    }
}

export const actions = {
    async getLoginMessage({ commit }, data) {
        await this.$axios.$post('http://localhost:5000/login', data).then(success => {
            commit('SET_LOGIN', success)
        }).catch(error => {
            commit('SET_LOGIN', error.response.data)
        });

    }
}