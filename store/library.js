export const state = () => ({
    library: null
})

export const mutations = {
    SET_LIBRARY(state, data) {
        state.library = data
    }
}

export const getters = {
    getterLibrary(state) {
        const library = state.library
        return library
    }
}

export const actions = {
    async getLibrary({ commit }, data) {
        const library = await this.$axios.$get('http://localhost:5000/library/'+data)
        commit('SET_LIBRARY', library)

    }
}