export const state = () => ({
    exer: null,
    exer_id:null
})

export const mutations = {
    SET_EXER(state, message) {
        state.exer = message
    },
    SET_EXER_BY_ID(state,exer){
        state.exer_id = exer
    }
}

export const getters = {
    getterAllExer(state) {
        if(state.exer == null){
            return "nothing"
        }else{
            const message = state.exer
            return message
        }
    },
    getterExer(state){
        if(state.exer_id == null){
            return "nothing"
        }else{
            const exer = state.exer_id
            return exer
        }
    }
}

export const actions = {
    async getAllExer({ commit }) {
        const data = await this.$axios.$get('http://localhost:5000/exer')
        commit('SET_EXER',data)
    },
    async getExer({ commit },id) {
        const data = await this.$axios.$get('http://localhost:5000/exer/'+id)
        commit('SET_EXER_BY_ID',data)
    }
}