'use strict'

const util = require('util')
const mysql = require('mysql')
const db = require('./../db')
const { response } = require('express')

module.exports = {
    get: (req, res) => {
        let sql = "SELECT * FROM homework WHERE id_ex = ?"
        db.query(sql, [req.params.id_ex], (err, response) => {
            if (err) throw err
            res.json(response[0])
        })
    },
    all: (req, res) => {
        let sql = "SELECT * FROM homework"
        db.query(sql, (err, response) => {
            if (err) throw err
            res.json(response)
        })
    },
    submit: (req, res) => {
        let sql = 'SELECT * FROM homework'
        try {
            db.query(sql, (err, response) => {
                if (err) return res.json({ message: "Insert failed" })
                let data = [...response]
                for (let index = 0; index < data.length; index++) {
                    const result = data[index];
                    if (result.file == req.body.file) {
                        res.json({
                            percent: 100,
                            location:result.id_sv
                        })
                    }
                }
            })
        } catch (error) {
            res.status(400).send(err);
        }

    }
}