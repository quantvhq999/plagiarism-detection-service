
const express = require('express')
const cors = require('cors');
const app = express()
const bodyParser = require('body-parser')

require('dotenv').config()
const port = process.env.PORT || 5000

app.use(cors());
app.options('*', cors());
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

let routes = require('./api/routes') //importing route
routes(app)

//default route 
app.get('/',(req,res)=>{
    return res.send({ message:'This route do nothing'})
});

//error url
app.use(function(req, res) {
    res.status(404).send({url: req.originalUrl + ' not found'})
})

app.listen(port)

console.log('RESTful API server started on: ' + port)