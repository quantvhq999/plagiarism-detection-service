'use strict';

const util = require('util')
const mysql = require('mysql')
const db = require('./db')
const { response } = require('express')
const removeStopWord= (arr)=>{
    const fsLibrary = require('fs')
    const dir = fsLibrary.readFileSync('D:/Project/intern/BE_API/api/stopwords.txt', 'utf8');
    
    const res =[]
    const stopwords = dir.normalize("NFD").replace(/[\u0300-\u036f]/g,'').replace(/['\r']/g,' ').split('\n')
    for (let index = 0; index < arr.length; index++) {
        if(!stopwords.includes(arr[index])) {
            res.push(arr[index])
        }
    }
    return(res)
}
const test = (text1, text2) => {
    let result = {}
    let duplicate = []
    let percentSum = 0
    const stopword = /[\u0300-\u036f,',','.','(',')','"']/g
    if ( text1.type == 'file') {
        const text1_change = text1.text.normalize("NFD").replace(stopword, "").replace(/['\r','\n']/g,' ').toLowerCase()
        for (let index = 0; index < text2.length; index++) {
            let text2_change = text2[index];
            let text2_item = text2_change.file.normalize("NFD").replace(stopword, "").replace(/['\r','\n']/g,' ').split(" ").join('').toLowerCase()
            if (text1_change == text2_item) {
                duplicate.push({
                    name: text2_change.name,
                    text: text2_change.file,
                    percent: 100
                })
            } else {
                text2_item = text2_change.file.normalize("NFD").replace(stopword, "").replace(/['\r','\n']/g,' ').toLowerCase()
                let text2_arr = text2_item.split(' ').filter((el)=>{
                    return el != ''
                })
                removeStopWord(text2_arr)
                let text2_arr_removed_stop_word = removeStopWord(text2_arr)
                let text1_arr = text1_change.split(' ').filter((el)=>{
                    return el != ''
                })
                let text1_arr_removed_stop_word = removeStopWord(text1_arr)
                const percent = (text1_arr_removed_stop_word.filter(value => text2_arr_removed_stop_word.includes(value)).length/text2_arr_removed_stop_word.length)*100
                if(percent > 20){
                    duplicate.push({
                        name : text2_change.name,
                        percent: ((text1_arr_removed_stop_word.filter(value => text2_arr_removed_stop_word.includes(value)).length/text2_arr_removed_stop_word.length)*100).toFixed(2)
                    })
                   
                    percentSum = percentSum + percent
                }
            }
            result.duplicate = duplicate
            if(percentSum > 100){
                result.percent = 100
            }else{
                result.percent = percentSum
            }
        }
        if(result.percent < 20 && text1.update == '0'){
            const datetime = new Date();
                    let sql = 'INSERT INTO homework SET ?'
                    let data = {
                        id_sv : text1.id_sv,
                        id_sub : text1.id_sub,
                        id_ex : text1.id_ex,
                        file : text1.text,
                        date : datetime.toISOString().slice(0,10),
                        done : 1
                    }
                    db.query(sql,[data],(err,response)=>{
                        if(err) throw err
                    })
        }if(result.percent < 20 && text1.update == '1'){
            const datetime = new Date();
            let sql = 'UPDATE homework SET ? WHERE id_sv='+ text1.id_sv + ' AND ' + 'id_ex='+ text1.id_ex
                    let data = {
                        id_sv : text1.id_sv,
                        id_sub : text1.id_sub,
                        id_ex : text1.id_ex,
                        file : text1.text,
                        date : datetime.toISOString().slice(0,10),
                        done : 1
                    }
                    db.query(sql,[data],(err,response)=>{
                        if(err) throw err
                    })
        }
        return result
    } else {
        const text1_change = text1.text.normalize("NFD").replace(stopword, "").replace(/['\r','\n']/g,' ').toLowerCase()
        for (let index = 0; index < text2.length; index++) {
            let text2_change = text2[index];
            let text2_item = text2_change.file.normalize("NFD").replace(stopword, "").replace(/['\r','\n']/g,' ').split(" ").join('').toLowerCase()
            if (text1_change == text2_item) {
                duplicate.push({
                    name: text2_change.name,
                    text: text2_change.file,
                    percent: 100
                })
            } else {
                text2_item = text2_change.file.normalize("NFD").replace(stopword, "").replace(/['\r','\n']/g,' ').toLowerCase()
                let text2_arr = text2_item.split(' ').filter((el)=>{
                    return el != ''
                })
                removeStopWord(text2_arr)
                let text2_arr_removed_stop_word = removeStopWord(text2_arr)
                let text1_arr = text1_change.split(' ').filter((el)=>{
                    return el != ''
                })
                let text1_arr_removed_stop_word = removeStopWord(text1_arr)
                const percent = (text1_arr_removed_stop_word.filter(value => text2_arr_removed_stop_word.includes(value)).length/text2_arr_removed_stop_word.length)*100
                if(percent > 20){
                    duplicate.push({
                        name : text2_change.name,
                        percent: ((text1_arr_removed_stop_word.filter(value => text2_arr_removed_stop_word.includes(value)).length/text2_arr_removed_stop_word.length)*100).toFixed(2)
                    })
                   
                    percentSum = percentSum + percent
                }
            }

        }
        result.duplicate = duplicate
        if(percentSum > 100){
            result.percent = 100
        }else{
            result.percent = percentSum
        }
    }
    return result
}
module.exports = test