'use strict'

const util = require('util')
const mysql = require('mysql')
const db = require('./../db')
const { response } = require('express')

module.exports = {
    get: (req, res) => {
        let sql = 'SELECT * FROM user'
        db.query(sql, (err, response) => {
            if (err) throw err
            res.json(response)
        })
    },
    detail: (req, res) => {
        let sql = 'SELECT * FROM user WHERE std_id = ?'
        db.query(sql, [req.params.UserId], (err, response) => {
            if (err) throw err
            res.json(response[0])
        })
    },
    store: (req, res) => {
        const data = req.body
        let sql = "INSERT INTO user SET ?"
        try{
            if(Object.keys(data).length === 0 && data.constructor === Object)
                res.status(400).send({message:"NULL body"})
            else
            db.query(sql, [data], (response)=>{
                res.json({message:"Insert success",name:data.name})
            })
        }catch(err){
            res.status(400).send(err);
        }
    },
    login:(req,res)=>{
        let sql = 'SELECT std_id,password FROM user WHERE std_id= ? AND password = ? '
        let login_data = req.body
        let data_sql = [req.body.std_id, req.body.password]
        db.query(sql, data_sql, (err, response) => {
            if (err) throw err
            if(Object.keys(login_data).length === 0 && login_data.constructor === Object){
                res.status(400).send({message:"LOGIN FAILED"})
            }else{
                const data = {...response[0]}
                if(data.std_id == login_data.std_id && data.password == login_data.password){
                    res.json({message:"LOGIN SUCCESS"})
                }else{
                    res.status(401).send({message:"LOGIN FAILED"})
                }
            }
        })
    }
}