export const state = () =>({
    info:null
})

export const mutations = {
    SET_INFOR(state,data){
        state.info = data
    }
}

export const getters = {
    getterUserInfor(state){
        if(state.info == null)
            return "nothing"
        else{
            const data = state.info
            return data
        }
    }
}

export const actions = {
    async getUserInfor({commit}){
        const id_sv = localStorage.getItem('std_id')
        const data = await this.$axios.get('http://localhost:5000/user/'+id_sv)
        commit('SET_INFOR',data.data)
    }
}