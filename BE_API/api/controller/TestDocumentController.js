'use strict'

const util = require('util')
const mysql = require('mysql')
const db = require('./../db')
const { response } = require('express')
const test = require('./../test')
module.exports = {
    test:(req,res)=>{
        let sql = "SELECT * FROM library"
        let text = req.body
        db.query(sql, [text], (err, response) => {
            if (err) throw err
            const data = response
            if(text == null || text == "" ){
                return res.status(400).send({ message: 'Null content' });
            }else{
                return res.json(test(text,data))
            }
        })
    }
}