export const state = ()=>({
    subjects:null,
    subject:null
})

export const mutations = {  
    SETSUBJECT(state,data){
        state.subjects = data
    },
    SET_SUBJECT(state,data){
        state.subject = data
    },
}

export const getters = {    
    getterSubjects(state){
        if(state.subjects == null){
            return "nothing"
        }else{
            const subject = state.subjects
            return subject
        }
    },
    getterSubject(state){
        if(state.subject == null){
            return "nothing"
        }else{
            const subject = state.subject
            return subject
        }
    }
}

export const actions={
    async getAllSubjects({commit}){
        const data = await this.$axios.$get('http://localhost:5000/subject')
        commit('SETSUBJECT',data)
    },
    async getSubjects({commit},id){
        const data = await this.$axios.$get('http://localhost:5000/subject/'+id)
        commit('SET_SUBJECT',data)
    }
}